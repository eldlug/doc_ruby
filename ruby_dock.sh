#!/usr/bin/env bash
#data: 23.06.21
#purpose: Install Ruby on Centos 7 
#version: v0.1.0
#help: https://www.linkedin.com/pulse/how-install-ruby-centos-7-amal-krishna-r?trk=public_profile_article_view
##################################

sudo yum install ruby

#ruby version - ruby 2.0.0p648 (2015-12-16) [x86_64-linux]

sudo yum install git libreadline-dev zlib1g-dev libreadline-dev libncurses5-dev autoconf bison libssl-dev build-essential libyaml-dev libffi-dev libssl-dev libreadline-dev zlib1g-dev libgdbm-dev

git clone https://github.com/rbenv/rbenv.git ~/.rbenv


echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build

echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc

exec $SHELL

rbenv install 2.6.1

rbenv global 2.6.1

ruby --version
#ruby 2.6.1 [x86_64-linux-gnu] - ruby 2.6.1p33 (2019-01-30 revision 66950) [x86_64-linux]

gem install bundler
gem install rails

sudo yum -y install postgresql-server postgresql-devel postgresql-contrib

sudo postgresql-setup initdb

sudo systemctl start postgresql
sudo systemctl enable postgresql

                                                      1,1           Top


